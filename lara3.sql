-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2021 at 02:06 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara3`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_desc` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_desc`, `brand_image`, `brand_status`, `created_at`, `updated_at`) VALUES
(11, 'Polosan', 'Original Brand From Us', 'admin/images/logo.PNG', 1, '2021-06-03 07:48:21', '2021-06-11 01:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_desc` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_desc`, `cat_image`, `cat_status`, `created_at`, `updated_at`) VALUES
(13, 'JACKET', NULL, NULL, 1, NULL, '2021-06-03 09:35:04'),
(14, 'SOCK', NULL, '', 1, '2021-06-03 08:11:10', '2021-06-03 09:35:05');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `commenter_id` int(11) NOT NULL,
  `commentable_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `commenter_id`, `commentable_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 17, '9', 'Nice product', '2019-12-29 23:58:30', '2019-12-29 23:58:30'),
(2, 17, '9', 'Wow', '2019-12-30 00:13:14', '2019-12-30 00:13:14'),
(3, 17, '11', 'Nice', '2019-12-30 00:23:35', '2019-12-30 00:23:35'),
(4, 17, '11', 'Very nice and wow product', '2019-12-30 00:25:33', '2019-12-30 00:25:33'),
(5, 17, '11', 'hi', '2019-12-30 00:28:20', '2019-12-30 00:28:20'),
(6, 17, '10', 'Nice product', '2019-12-30 00:29:18', '2019-12-30 00:29:18'),
(7, 21, '13', 'Well', '2019-12-30 00:39:00', '2019-12-30 00:39:00'),
(8, 21, '9', 'Not Bad', '2019-12-30 00:39:30', '2019-12-30 00:39:30'),
(9, 21, '9', 'good', '2019-12-30 00:43:56', '2019-12-30 00:43:56');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email_address`, `phone_no`, `provider`, `provider_id`, `password`, `address`, `created_at`, `updated_at`) VALUES
(17, 'Yeamin', 'Hossain', 'yeamin510@gmail.com', '01750691974', '', '', '$2y$10$Vq9oK9xA4M/izE1kEuQNTer6KMoKlQLjO7DrqZbwpFiEXsXiuy2ty', NULL, '2019-12-18 01:24:41', '2019-12-18 01:24:41'),
(21, 'Abdur', 'Karim', 'karim@gmail.com', '01829409162', '', '', '$2y$10$aYPf.IE99LCrVQPdGj109.SHm9cqNHnMGZxi8io7/qKLE.Oueptl.', 'Uttara', '2019-12-21 14:12:39', '2019-12-21 14:12:39'),
(22, 'Rahim', 'Mia', 'rahim@gmail.com', '01650321456', '', '', '$2y$10$pAk08QZI7TLQX6l.4iCgIuNKWi4d90mxLv5kf7u.X9DsqJQz9mVcW', NULL, '2019-12-21 14:29:43', '2019-12-21 14:29:43'),
(23, 'Rakib', 'Uddin', 'rakib@gmail.com', '01750', '', '', '$2y$10$cepRFB9ftgm2cErwJ4s8XeDWiNYRmmG5E0dLG9Ju9JHJeYOueUK9m', NULL, '2019-12-21 14:31:19', '2019-12-21 14:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_19_175400_create_categories_table', 1),
(4, '2019_11_23_052509_create_brands_table', 2),
(5, '2019_12_03_160921_create_products_table', 3),
(6, '2018_12_23_120000_create_shoppingcart_table', 4),
(7, '2019_12_17_043042_create_customers_table', 5),
(9, '2019_12_21_165659_create_orders_table', 7),
(10, '2019_12_21_170416_create_order_details_table', 7),
(11, '2019_12_21_170514_create_payments_table', 7),
(13, '2019_12_21_111701_create_shipping_addresses_table', 8),
(14, '2014_10_12_000000_create_users_table', 9),
(15, '2019_12_30_051724_create_comments_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `order_total` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `shipping_id`, `order_total`, `order_status`, `created_at`, `updated_at`) VALUES
(3, 17, 2, '1,000.00', 'pending', '2019-12-21 14:00:36', '2019-12-21 14:00:36'),
(4, 17, 3, '3,000.00', 'pending', '2019-12-21 14:03:43', '2019-12-21 14:03:43'),
(5, 21, 1, '4,000.00', 'pending', '2019-12-21 14:15:07', '2019-12-21 14:15:07'),
(6, 21, 2, '100.00', 'pending', '2019-12-21 14:17:26', '2019-12-21 14:17:26'),
(7, 21, 3, '500.00', 'pending', '2019-12-21 14:27:20', '2019-12-21 14:27:20'),
(8, 23, 4, '6,000.00', 'pending', '2019-12-21 14:32:54', '2019-12-21 14:32:54'),
(9, 17, 5, '1,500.00', 'pending', '2019-12-29 00:53:13', '2019-12-29 00:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `product_name`, `product_price`, `product_quantity`, `created_at`, `updated_at`) VALUES
(2, 3, 10, 'Mobile', 1000.00, 1, '2019-12-21 14:00:36', '2019-12-21 14:00:36'),
(3, 4, 11, 'Shoe', 1500.00, 2, '2019-12-21 14:03:43', '2019-12-21 14:03:43'),
(4, 5, 13, 'Full Sleeve Casual Shirt', 2000.00, 1, '2019-12-21 14:15:07', '2019-12-21 14:15:07'),
(5, 5, 10, 'Mobile', 1000.00, 2, '2019-12-21 14:15:07', '2019-12-21 14:15:07'),
(6, 6, 9, 'Headphone', 100.00, 1, '2019-12-21 14:17:26', '2019-12-21 14:17:26'),
(7, 7, 9, 'Headphone', 100.00, 5, '2019-12-21 14:27:21', '2019-12-21 14:27:21'),
(8, 8, 13, 'Full Sleeve Casual Shirt', 2000.00, 3, '2019-12-21 14:32:54', '2019-12-21 14:32:54'),
(9, 9, 11, 'Shoe', 1500.00, 1, '2019-12-29 00:53:13', '2019-12-29 00:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('yeamin510@gmail.com', '$2y$10$LQMz/aUKWerH1shKfoj3EeYVWBg5JW6kpWJvhtSrZ8XXT9mNQF1U2', '2019-12-17 00:45:58'),
('yeamin510@gmail.com', '$2y$10$LQMz/aUKWerH1shKfoj3EeYVWBg5JW6kpWJvhtSrZ8XXT9mNQF1U2', '2019-12-17 00:45:58');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `payment_info`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 2, 'cash', 'pending', '2019-12-21 13:59:02', '2019-12-21 13:59:02'),
(2, 3, 'cash', 'pending', '2019-12-21 14:00:36', '2019-12-21 14:00:36'),
(3, 4, 'cash', 'pending', '2019-12-21 14:03:43', '2019-12-21 14:03:43'),
(4, 5, 'cash', 'pending', '2019-12-21 14:15:07', '2019-12-21 14:15:07'),
(5, 6, 'cash', 'pending', '2019-12-21 14:17:26', '2019-12-21 14:17:26'),
(6, 7, 'cash', 'pending', '2019-12-21 14:27:21', '2019-12-21 14:27:21'),
(7, 8, 'cash', 'pending', '2019-12-21 14:32:54', '2019-12-21 14:32:54'),
(8, 9, 'cash', 'pending', '2019-12-29 00:53:13', '2019-12-29 00:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_price` double(10,2) DEFAULT NULL,
  `product_price` double(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gallery_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `uploaded_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `brand_id`, `product_name`, `short_desc`, `long_desc`, `discount_price`, `product_price`, `quantity`, `size`, `image`, `gallery_image`, `status`, `uploaded_by`, `created_at`, `updated_at`) VALUES
(15, 14, 11, 'Kaos Polos Cotton Combed 30s', 'Kaos Polos Cotton Combed 30s\r\nLengan pendek - Size S', 'Kaos Polos Cotton Combed 30s\r\n.\r\nUnisex (Men / Women)\r\nReady Size S - M - L - XL\r\n.\r\nORDER NOW!!!\r\nWhatsApp : +62 8585 9598 519\r\nTokopedia : Plain Store.co', 0.00, 60000.00, 100, 's', 'admin/images/product_images/20210603151240_CCpendek.PNG', '[\"admin\\/images\\/product_images\\/CCpendek.PNG\"]', 1, '1', '2021-06-03 08:12:40', '2021-06-03 08:12:40'),
(16, 14, 11, 'Kaos Stripe Maroon', 'Kaos Stripe Cotton Comed 30s\r\nLengan Pendek\r\nSize XL', 'Kaos Stripe (BIG)\r\nCotton Combed 30s\r\n.\r\nUnisex (Men / Women)\r\nReady Size S - M - L - XL\r\n.\r\nORDER NOW!!!\r\nWhatsApp : +62 8585 9598 519\r\nTokopedia : Plain Store.co', 0.00, 80000.00, 50, 'xl', 'admin/images/product_images/20210603155513_STRIPEMAROON.PNG', '[\"admin\\/images\\/product_images\\/STRIPE.PNG\"]', 1, '6', '2021-06-03 08:55:13', '2021-06-03 08:55:13');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `customer_id`, `full_name`, `email_address`, `phone_no`, `address`, `created_at`, `updated_at`) VALUES
(1, 21, 'Abdur Karim', 'karim@gmail.com', '01829409162', 'Uttara', '2019-12-21 14:15:00', '2019-12-21 14:15:00'),
(2, 21, 'Abdur Karim', 'karim@gmail.com', '01750', 'Uttara', '2019-12-21 14:17:22', '2019-12-21 14:17:22'),
(3, 21, 'Abdur Karim', 'karim@gmail.com', '01829409162', 'Uttara', '2019-12-21 14:27:17', '2019-12-21 14:27:17'),
(4, 23, 'Rakib Uddin', 'rakib@gmail.com', '01750', 'dhaka', '2019-12-21 14:32:47', '2019-12-21 14:32:47'),
(5, 17, 'Yeamin Hossain', 'yeamin510@gmail.com', '01750691974', 'Uttara', '2019-12-29 00:53:03', '2019-12-29 00:53:03'),
(6, 17, 'Yeamin Hossain', 'yeamin510@gmail.com', '01750691974', 'Dhaka', '2019-12-29 00:54:39', '2019-12-29 00:54:39'),
(7, 21, 'Abdur Karim', 'karim@gmail.com', '01829409162', 'Uttara', '2019-12-30 00:44:23', '2019-12-30 00:44:23'),
(8, 21, 'Abdur Karim', 'karim@gmail.com', '01829409162', 'Uttara', '2019-12-30 00:54:09', '2019-12-30 00:54:09'),
(9, 21, 'Abdur Karim', 'karim@gmail.com', '01829409162', 'Uttara', '2019-12-30 00:57:52', '2019-12-30 00:57:52');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`identifier`, `instance`, `content`, `created_at`, `updated_at`) VALUES
('username2', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:2:{s:32:\"041046abcadc49d6f2e8e0d2a118e200\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":10:{s:5:\"rowId\";s:32:\"041046abcadc49d6f2e8e0d2a118e200\";s:2:\"id\";i:9;s:3:\"qty\";i:1;s:4:\"name\";s:9:\"Headphone\";s:5:\"price\";d:100;s:6:\"weight\";d:0;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:6:\"images\";s:53:\"admin/images/product_images/20191207185149_images.jpg\";}}s:7:\"taxRate\";i:21;s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:46:\"\0Gloudemans\\Shoppingcart\\CartItem\0discountRate\";i:0;}s:32:\"2e90d778a6aa0b768e18b0c27876086e\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":10:{s:5:\"rowId\";s:32:\"2e90d778a6aa0b768e18b0c27876086e\";s:2:\"id\";i:11;s:3:\"qty\";i:1;s:4:\"name\";s:4:\"Shoe\";s:5:\"price\";d:1500;s:6:\"weight\";d:0;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:6:\"images\";s:52:\"admin/images/product_images/20191215174838_f-p-1.jpg\";}}s:7:\"taxRate\";i:0;s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:46:\"\0Gloudemans\\Shoppingcart\\CartItem\0discountRate\";i:0;}}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL DEFAULT 3,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `phone`, `email`, `email_verified_at`, `address`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Zaenatur R. Roifah', '082140923379', 'zae@gmail.com', NULL, 'TRENGGALEK', '$2y$10$jHlj.J.IpeVEjTpB96CWGuS5UVzsq/y.7iE8RNN7BlYNVo8LPZavy', '1', 'bHB8dJ7PbxcBoLA7WE14PWxl7meYWByMLFjYdqkiyUm2V4yjwzhMq1g7rBHK', '2019-12-22 08:53:46', '2019-12-22 08:53:46'),
(6, 1, 'Dimas Suhro Ardi', '085859598519', 'dimassuhro@gmail.com', NULL, 'LAMONGAN', '$2y$10$3ZWrjDODBQ9ljrM0OcG9beD865VdWJS4.3snwjwqKfGrEsHpev/7e', '1', NULL, '2021-06-03 07:27:43', '2021-06-03 07:27:43'),
(7, 2, 'M. Ircham Maulana', '085731247524', 'irchammaulana@gmail.com', NULL, 'LAMONGAN', '$2y$10$sKLUUpQZpOuYm07Dv6fzDuOUT1WBqzrgQVmk4OGAskuBHZxny5FW6', '1', NULL, '2021-06-03 07:32:46', '2021-06-03 07:32:46'),
(8, 2, 'M. Sulthon Robbani', '082132407812', 'msulthon@gmail.com', NULL, 'LAMONGAN', '$2y$10$wrYmzfoBl2aLkiG0evUKfOav0nuSh6YJZJOsiaKh.XeCsNbKO6L1m', '1', NULL, '2021-06-03 07:33:43', '2021-06-03 07:33:43'),
(9, 2, 'M. Rijal Amirudin', '08819359604', 'mramirudin@gmail.com', NULL, 'SIDOARJO', '$2y$10$.XKT85pDJ4EIcNDJayzcu.hRdp6OCJvjQmE7FtckNME.P332ZXkOG', '1', NULL, '2021-06-03 07:34:32', '2021-06-03 07:34:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_brand_name_unique` (`brand_name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_cat_name_unique` (`cat_name`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_address_unique` (`email_address`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
