@extends('admin.master')

@section('body')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <span class="h2">Informasi Pesanan</span>
                                <a href="{{route('order.invoice', $orderDetails->id)}}" class="btn btn-primary float-right">
                                    Cetak Pesanan
                                </a>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>No. Pesanan</th>
                                        <td>{{$orderDetails->id}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Pesanan</th>
                                        <td>Rp. {{$orderDetails->order_total}}</td>
                                    </tr>
                                    <tr>
                                        <th>Status Pesanan</th>
                                        <td>{{$orderDetails->order_status}}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Pesanan</th>
                                        <td>{{ date("d M Y - h:i:sa", strtotime($orderDetails->created_at)) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Informasi Pelanggan</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Nama Pelanggan</th>
                                        <td>{{$orderDetails->customer->first_name.' '.$orderDetails->customer->last_name}}</td>
                                    </tr>
                                    <tr>
                                        <th>No. Telvon</th>
                                        <td>{{$orderDetails->customer->phone_no}}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Email</th>
                                        <td>{{$orderDetails->customer->email_address}}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat</th>
                                        <td>{{$orderDetails->customer->address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Informasi Pengiriman</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <td>{{$orderDetails->shipping->full_name}}</td>
                                    </tr>
                                    <tr>
                                        <th>No. Telvon</th>
                                        <td>{{$orderDetails->shipping->phone_no}}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Email</th>
                                        <td>{{$orderDetails->shipping->email_address}}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat</th>
                                        <td>{{$orderDetails->shipping->address}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Informasi Pembayaran</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Status Pembayaran</th>
                                        <td>{{$orderDetails->payment->payment_status}}</td>
                                    </tr>
                                    <tr>
                                        <th>Tipe Pembayaran</th>
                                        <td>{{$orderDetails->payment->payment_info}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Informasi Produk</h2>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>No.</th>
                                        <th>Id Produk</th>
                                        <th>Nama Produk</th>
                                        <th>Harga Produk</th>
                                        <th>Quantitas Produk</th>
                                        <th>Total Harga</th>
                                    </tr>
                                    @php($sl = 1)
                                    @foreach($productDetails as $productDetail)
                                    <tr>
                                        <td>{{$sl++}}</td>
                                        <td>{{$productDetail->product_id}}</td>
                                        <td>{{$productDetail->product_name}}</td>
                                        <td>Tk. {{$productDetail->product_price}}</td>
                                        <td>{{$productDetail->product_quantity}}</td>
                                        <td>Rp. {{$productDetail->product_quantity*$productDetail->product_price}}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection